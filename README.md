# Api-repuestos-mongo-en-gitlab

Mini ApiRest con FastApi y MongoDB

## Documentación oficial

https://fastapi.tiangolo.com/

## Comando para crear un espacio virtual

virtualenv env

## Comando para activar virtualenv Windows y Linux

env\scripts\activate

source venv/bin/activate

source env/bin/activate

## Comando Para Crear requeriments.txt

pip freeze > requeriments.txt

## Comando Para Activar o Instalar los paquetes en requeriments.txt

pip install -r requeriments.txt

## Instalación De FastApi

pip install fastapi

## También vas a necesitar un servidor ASGI para producción cómo Uvicorn

pip install uvicorn[standard]

## Corre el servidor con

uvicorn main:app --reload

## El entorno virtual lo hago directamente con Pycharm

## Cambiar de rama master a main con git es con el siguiente scripts

git branch -M main


